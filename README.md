# recipe-app-api-proxy

nginx app proxy for our api
## Usage

### Environment Variables 

* LISTEN-PORT = default port : 8000
* APP-HOST = Host name of the app to forward requests (default= 'app' )
* APP-PORT = Port of the app to forward requests ( default= '9000' )
